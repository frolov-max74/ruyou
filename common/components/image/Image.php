<?php

namespace common\components\image;

use yii\base\Component;
use yii\imagine\Image as Img;

class Image extends Component
{
    public $pathValues;
    public $imageValues;

    /**
     * Image constructor.
     * @param PathValues $pathValues
     * @param ImageValues $imageValues
     * @param array $config
     */
    public function __construct(PathValues $pathValues, ImageValues $imageValues, array $config = [])
    {
        $this->pathValues = $pathValues;
        $this->imageValues = $imageValues;

        parent::__construct($config);
    }

    /**
     * return bool
     */
    public function resize(): bool
    {
        if (!file_exists($this->pathValues->getSourcePath())) {
            return false;
        }
        Img::thumbnail(
            $this->pathValues->getSourcePath(), $this->imageValues->getWidth(), $this->imageValues->getHeight()
        )->save($this->pathValues->getDestinationPath(), ['quality' => $this->imageValues->getQuality()]);
        return true;
    }
}

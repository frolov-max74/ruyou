<?php

namespace common\components\image;

/**
 * Class ImageValues
 * @package common\components\image
 */
class ImageValues
{
    private $width;
    private $height;
    private $quality;

    /**
     * ImageValues constructor.
     * @param int $width
     * @param int $height
     * @param int $quality
     */
    public function __construct(int $width, int $height, int $quality = 80)
    {
        $this->width = $width;
        $this->height = $height;
        $this->quality = $quality;
    }

    /**
     * Gets the width of the image
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * Gets the height of the image
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * Gets image quality in percent
     */
    public function getQuality(): int
    {
        return $this->quality;
    }
}

<?php

namespace common\helpers;

use common\models\Request;
use yii\helpers\Html;

/**
 * Class RequestHelper
 * @package common\helpers
 */
class RequestHelper extends Helper
{
    /**
     * Returns an array of pairs (status value => name).
     * @return array
     */
    public static function statusList(): array
    {
        return [
            Request::OPTION_WEB => 'WEB-ПРОЕКТ',
            Request::OPTION_MOBILE => 'MOBILE-ПРОЕКТ',
            Request::OPTION_OTHER => 'АВТОМАТИЗАЦИЯ',
        ];
    }

    /**
     * Returns a span tag with a value and class depending on the status passed.
     * @param $status
     * @return string
     */
    public static function statusLabel($status): string
    {
        switch ($status) {
            case Request::OPTION_WEB:
                $class = 'label label-primary';
                break;
            case Request::OPTION_MOBILE:
                $class = 'label label-success';
                break;
            case Request::OPTION_OTHER:
                $class = 'label label-default';
                break;
            default:
                $class = 'label label-info';
        }

        return Html::tag('span', self::statusName($status), [
            'class' => $class,
        ]);
    }
}

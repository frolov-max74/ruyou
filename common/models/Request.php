<?php

namespace common\models;

use Yii;
use yii\helpers\HtmlPurifier;
use common\components\image\{
    Image, ImageValues, PathValues
};
use yii\{
    db\ActiveRecord, web\UploadedFile, behaviors\TimestampBehavior
};

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $middle_name
 * @property string $message
 * @property int $created_at
 * @property int $option
 * @property string $image
 */
class Request extends ActiveRecord
{
    const OPTION_WEB = 0;
    const OPTION_MOBILE = 1;
    const OPTION_OTHER = 2;

    public $imageFile;
    protected $basePath;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%request}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['last_name', 'first_name', 'message', 'option'], 'required'],
            [['last_name', 'first_name', 'middle_name'], 'string', 'max' => 50],
            [['message'], 'string'],
            [
                ['last_name', 'first_name', 'middle_name'],
                'match',
                'pattern' => '/^[а-яёa-z]+$/iu',
                'message' => 'Пожалуйста, указывайте только буквы.'
            ],
            [['last_name', 'first_name', 'middle_name', 'message'], 'trim'],
            [
                ['message'], 'filter', 'filter' => function ($value) {
                return HtmlPurifier::process($value);
            },
            ],
            [['last_name', 'first_name', 'middle_name'], 'filter', 'filter' => [$this, 'normalizeFio']],
            [['option'], 'integer'],
            ['option', 'default', 'value' => self::OPTION_WEB],
            ['option', 'in', 'range' => [self::OPTION_WEB, self::OPTION_MOBILE, self::OPTION_OTHER]],
            [['image'], 'string', 'max' => 100],
            [['image'], 'default', 'value' => ''],
            [
                ['imageFile'],
                'file',
                'maxSize' => 2048 * 2048,
                'extensions' => 'png, jpg, jpeg',
                'checkExtensionByMimeType' => false,
            ],
        ];
    }

    /**
     * @param $value
     * @param string $encoding
     * @return bool|mixed|string
     */
    public function normalizeFio($value, $encoding = 'UTF-8')
    {
        $value = mb_strtolower($value, $encoding);
        $value = mb_strtoupper(mb_substr($value, 0, 1, $encoding), $encoding) .
            mb_substr($value, 1, null, $encoding);
        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'middle_name' => 'Отчество',
            'message' => 'Сообщение',
            'created_at' => 'Время заявки',
            'option' => 'Опция',
            'imageFile' => 'Изображение',
            'image' => 'Изображение',
        ];
    }

    /**
     * Upload and resizing request image
     */
    public function uploadImage()
    {
        $this->basePath = Yii::getAlias('@frontend/web');

        if ($this->imageFile = UploadedFile::getInstance($this, 'imageFile')) {
            $relPath = '/upload/images/requests/' .
                $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $destPath = $this->basePath . $relPath;
            if ($this->imageFile->saveAs($destPath)) {
                if ($this->image) {
                    $this->deleteImageFile($this->image);
                }
                $this->image = $relPath;
            }
            $this->resizeImage($relPath);
        }
    }

    /**
     * Resizing request image
     * @param string $relPath
     */
    protected function resizeImage(string $relPath)
    {
        $thumbPath = '/upload/images/requests/thumb_' . time() . '_' . rand(0, 999999) . '_' .
            $this->imageFile->baseName . '.' . $this->imageFile->extension;
        $srcPath = $this->basePath . $relPath;
        $destPath = $this->basePath . $thumbPath;
        $image = new Image(new PathValues($srcPath, $destPath), new ImageValues(900, 600));
        if ($image->resize()) {
            $this->deleteImageFile($relPath);
            $this->image = $thumbPath;
        }
    }

    /**
     * Before deleting a record deletes the image file
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteImageFile($this->image);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes the image file
     * @var string $path
     */
    protected function deleteImageFile(string $path)
    {
        if (is_file($filename = Yii::getAlias('@frontend/web' . $path))) {
            unlink($filename);
        }
    }
}

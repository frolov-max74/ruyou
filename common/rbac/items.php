<?php
return [
    'viewRequest' => [
        'type' => 2,
        'description' => 'View Request',
    ],
    'updateRequest' => [
        'type' => 2,
        'description' => 'Update Request',
    ],
    'deleteRequest' => [
        'type' => 2,
        'description' => 'Delete Request',
    ],
    'user' => [
        'type' => 1,
        'description' => 'User',
        'ruleName' => 'isUserActive',
        'children' => [
            'viewRequest',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Administrator',
        'ruleName' => 'isUserActive',
        'children' => [
            'updateRequest',
            'deleteRequest',
            'user',
        ],
    ],
];

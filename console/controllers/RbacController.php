<?php

namespace console\controllers;

use Yii;
use common\rbac\rules\UserRoleRule;
use yii\{
    console\Controller, helpers\Console
};

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // removed old data
        $auth->removeAll();

        // added permission "viewRequest"
        $viewRequest = $auth->createPermission('viewRequest');
        $viewRequest->description = 'View Request';
        $auth->add($viewRequest);

        // added permission "updateRequest"
        $updateRequest = $auth->createPermission('updateRequest');
        $updateRequest->description = 'Update Request';
        $auth->add($updateRequest);

        // added permission "deleteRequest"
        $deleteRequest = $auth->createPermission('deleteRequest');
        $deleteRequest->description = 'Delete Request';
        $auth->add($deleteRequest);

        // added rule "UserRoleRule"
        $rule = new UserRoleRule();
        $auth->add($rule);

        // added the role of "user" and is bound to her rule "UserRoleRule"
        $user = $auth->createRole('user');
        $user->description = 'User';
        $user->ruleName = $rule->name;
        $auth->add($user);

        // added the role of "admin" and is bound to her rule "UserRoleRule"
        $admin = $auth->createRole('admin');
        $admin->description = 'Administrator';
        $admin->ruleName = $rule->name;
        $auth->add($admin);

        // added permissions to roles
        $auth->addChild($user, $viewRequest);
        $auth->addChild($admin, $updateRequest);
        $auth->addChild($admin, $deleteRequest);

        // also all the permissions of "user" to the "admin"
        $auth->addChild($admin, $user);

        // assigned roles to users
        $auth->assign($admin, 1);
        // ... here assign roles to other users

        $this->stdout('Done!' . PHP_EOL, Console::FG_CYAN, Console::UNDERLINE);
    }
}

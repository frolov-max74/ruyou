<?php

use common\helpers\RequestHelper;
use yii\{
    helpers\Html, widgets\ActiveForm
};

/* @var $this yii\web\View */
/* @var $model common\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

    <?= $form
        ->field($model, 'option')
        ->dropDownList(
            RequestHelper::statusList(),
            ['prompt' => 'Выбрать']
        ) ?>

    <?= $model->image ? Html::img(
        Yii::$app->params['baseUrl'] . $model->image,
        ['class' => 'img-responsive', 'style' => 'width: 300px']
    ) : ''; ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? 'Отправить' : 'Обновить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use kartik\daterange\DateRangePicker;
use common\helpers\RequestHelper;
use yii\{
    helpers\Html, grid\GridView, widgets\Pjax
};

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Сбросить', ['index'], ['class' => 'btn btn-default']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'last_name',
            'first_name',
            'middle_name',
            'message:ntext',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y H:i'],
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'language' => 'ru',
                    'convertFormat' => true,
                    'startAttribute' => 'date_from',
                    'endAttribute' => 'date_to',
                    'pluginOptions' => [
                        'timePicker' => true,
                        'timePickerIncrement' => 15,
                        'locale' => [
                            'format' => 'd.m.Y H:i'
                        ]
                    ]
                ]),
            ],
            [
                'attribute' => 'option',
                'format' => 'raw',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'option',
                    RequestHelper::statusList(),
                    ['prompt' => 'Выбрать']
                ),
                'value' => function ($searchModel) {
                    return RequestHelper::statusLabel($searchModel->option);
                },
            ],
            [
                'attribute' => 'image',
                'enableSorting' => false,
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->image ? Html::img(
                        Yii::$app->params['baseUrl'] . $data->image,
                        [
                            'class' => 'img-responsive',
                            'style' => 'width: 150px',
                        ]
                    ) : '';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                //'template' => '{view} {update} {delete}',
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('viewRequest'),
                    'update' => Yii::$app->user->can('updateRequest'),
                    'delete' => Yii::$app->user->can('deleteRequest'),
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

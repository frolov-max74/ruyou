<?php

use common\helpers\RequestHelper;
use yii\{
    helpers\Html, widgets\DetailView, web\YiiAsset
};

/* @var $this yii\web\View */
/* @var $model common\models\Request */

$this->title = 'Заявка: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
YiiAsset::register($this);
?>
<div class="request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Yii::$app->user->can('updateRequest') ?
            Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= Yii::$app->user->can('deleteRequest') ? Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту заявку?',
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'last_name',
            'first_name',
            'middle_name',
            'message:ntext',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y H:i'],
            ],
            [
                'attribute' => 'option',
                'value' => function ($model) {
                    return RequestHelper::statusLabel($model->option);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->image ? Html::img(
                        Yii::$app->params['baseUrl'] . $data->image,
                        [
                            'class' => 'img-responsive',
                            'style' => 'width: 500px',
                        ]
                    ) : '';
                }
            ],
        ],
    ]) ?>

</div>
